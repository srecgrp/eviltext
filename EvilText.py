"""EvilText Adversarial Text Generation Framework:
----------------------------------------------------
This file contains the basic functions for perturbing a given input text using different
Text Perturbation Tactics (TPTs). The user just selects the preferred method for perturbing a given text
by giving the number before the TPT description. Next the user is asked to give the legitimate/original
text to be perturbed with the selected method in 1 line (i.e., without new lines). Finally, the EvilText
outputs the perturbed text to be used for evading some of the most popular NLP machines used for
sentiment and toxic content detection. Enjoy :)"""

import random
from nltk.stem import PorterStemmer

"""This function is used to read the Alphabet_Map/FINAL_MAP.txt file that contains
the map for perturbing the text given. The final_map list contains the unicode similar characters 
for each character of the 26 contained in the english alphabet."""
def read_map():
    with open("Alphabet_Map/FINAL_MAP.txt") as f:
        array = []
        lines = f.readlines()
        for line in lines:
            line = line.split(":")[1]
            line = line.strip("\n")
            line = line.strip(" ")
            array.append(line)
    final_map=[]
    for record in array:
        line = record.split(",")
        final_map.append(line)

    #print(final_map)
    return final_map


"""This is an attack where we replace only the negative sentiment words found in negative-words list."""
def replace_only_bad_sentiment_words_neg(final_map,review):
    #initialize Porter Stemmer
    ps = PorterStemmer()

    # read the negative sentiment words in opinion_words/negative_sentiment_words/negative-words.txt
    with open("opinion_words/negative_sentiment_words/negative-words.txt") as f:
        negative_sentiment_words = f.readlines()
    neg_local = []
    for word in negative_sentiment_words:
        neg_local.append(word.strip("\n"))

    negative_sentiment_words=neg_local

    # read the stemmed negative sentiment words in opinion_words/negative_sentiment_words/stemmed-negative-words.txt
    with open("opinion_words/negative_sentiment_words/stemmed-negative-words.txt") as fstemmed:
        negative_sentiment_words_stemmed = fstemmed.readlines()
    neg_local_stemmed = []
    for word in negative_sentiment_words_stemmed:
        neg_local_stemmed.append(word.strip("\n"))

    negative_sentiment_words_stemmed=neg_local_stemmed

    perturbed_text = []
    review = review.split("\n")

    for line in review:
        #split line in words
        words = line.split(" ")
        for word in words:
            neg_word_found_flag=0
            if len(word)==1:
                perturbed_text.append(word)
            else:
                neg_word_counter=0
                for neg_word in negative_sentiment_words:
                    #if neg_word==word:
                    #if neg_word==word or ps.stem(neg_word)==ps.stem(word):
                    if neg_word == word or negative_sentiment_words_stemmed[neg_word_counter] == ps.stem(word):
                        #print(neg_word+" ==   "+word)
                        #print(ps.stem(neg_word)+"  "+ps.stem(word))
                        neg_word_found_flag=1
                        #negative sentiment word found so perturb it and write it to file
                        temp_word = ""
                        for char in word:
                            if char >= 'A' and char <= 'Z':
                                # find the sublist pointer from the final_map list.
                                pointer = ord(char) - ord('A')
                                # get random char from the final_map to replace the original char.
                                random_choice = random.randrange(0, len(final_map[pointer]))

                                # get chosen hex num
                                chosen_char = str(final_map[pointer][random_choice].strip(" "))

                                # append 0x to hex num string to convert it.
                                chosen_char = "0x" + chosen_char
                                # get the actual integer of the specific hex num with base 16.
                                chosen_char = int(chosen_char, base=16)
                                # finally get the actual character stored for specific hex char representation.
                                chosen_char = chr(chosen_char)

                                # write modified char to perturbed file.
                                temp_word=temp_word+chosen_char

                            elif char >= 'a' and char <= 'z':
                                # find the sublist pointer from the final_map list.
                                pointer = ord(char) - ord('a')
                                # get random char from the final_map to replace the original char.
                                random_choice = random.randrange(0, len(final_map[pointer]))

                                # get chosen hex num
                                chosen_char = str(final_map[pointer][random_choice].strip(" "))

                                # append 0x to hex num string to convert it.
                                chosen_char = "0x" + chosen_char
                                # get the actual integer of the specific hex num with base 16.
                                chosen_char = int(chosen_char, base=16)
                                # finally get the actual character stored for specific hex char representation.
                                chosen_char = chr(chosen_char)

                                # write modified char to perturbed file.
                                temp_word=temp_word+chosen_char

                            else:
                                # other type of character so write it to file as it is.
                                temp_word=temp_word+char

                        perturbed_text.append(temp_word)
                        break
                    neg_word_counter=neg_word_counter+1

                if neg_word_found_flag==0:
                     perturbed_text.append(word)
            if word!="\n":
                perturbed_text.append(" ")
    return "".join(perturbed_text)



"""This is an attack where we replace only the negative sentiment words found in negative-words list."""
def replace_only_good_sentiment_words_pos(final_map,review):
    #initialize Porter Stemmer
    ps = PorterStemmer()

    # read the negative sentiment words in opinion_words/negative_sentiment_words/negative-words.txt
    with open("opinion_words/positive_sentiment_words/positive-words.txt") as f:
        positive_sentiment_words = f.readlines()
    neg_local = []
    for word in positive_sentiment_words:
        neg_local.append(word.strip("\n"))

    positive_sentiment_words=neg_local

    # read the stemmed negative sentiment words in opinion_words/negative_sentiment_words/stemmed-negative-words.txt
    with open("opinion_words/positive_sentiment_words/stemmed-positive-words.txt") as fstemmed:
        positive_sentiment_words_stemmed = fstemmed.readlines()
    neg_local_stemmed = []
    for word in positive_sentiment_words_stemmed:
        neg_local_stemmed.append(word.strip("\n"))

    positive_sentiment_words_stemmed=neg_local_stemmed

    perturbed_text = []
    review = review.split("\n")


    for line in review:
        #split line in words
        words = line.split(" ")
        for word in words:
            pos_word_found_flag=0
            if len(word)==1:
                perturbed_text.append(word)
            else:
                pos_word_counter=0
                for pos_word in positive_sentiment_words:
                    #if neg_word==word:
                    #if neg_word==word or ps.stem(neg_word)==ps.stem(word):
                    if pos_word == word or positive_sentiment_words_stemmed[pos_word_counter] == ps.stem(word):
                        #print(neg_word+" ==   "+word)
                        #print(ps.stem(neg_word)+"  "+ps.stem(word))
                        pos_word_found_flag=1
                        temp_word =""
                        #negative sentiment word found so perturb it and write it to file
                        for char in word:
                            if char >= 'A' and char <= 'Z':
                                # find the sublist pointer from the final_map list.
                                pointer = ord(char) - ord('A')
                                # get random char from the final_map to replace the original char.
                                random_choice = random.randrange(0, len(final_map[pointer]))

                                # get chosen hex num
                                chosen_char = str(final_map[pointer][random_choice].strip(" "))

                                # append 0x to hex num string to convert it.
                                chosen_char = "0x" + chosen_char
                                # get the actual integer of the specific hex num with base 16.
                                chosen_char = int(chosen_char, base=16)
                                # finally get the actual character stored for specific hex char representation.
                                chosen_char = chr(chosen_char)

                                # write modified char to perturbed file.
                                temp_word= temp_word +chosen_char

                            elif char >= 'a' and char <= 'z':
                                # find the sublist pointer from the final_map list.
                                pointer = ord(char) - ord('a')
                                # get random char from the final_map to replace the original char.
                                random_choice = random.randrange(0, len(final_map[pointer]))

                                # get chosen hex num
                                chosen_char = str(final_map[pointer][random_choice].strip(" "))

                                # append 0x to hex num string to convert it.
                                chosen_char = "0x" + chosen_char
                                # get the actual integer of the specific hex num with base 16.
                                chosen_char = int(chosen_char, base=16)
                                # finally get the actual character stored for specific hex char representation.
                                chosen_char = chr(chosen_char)

                                # write modified char to perturbed file.
                                temp_word= temp_word +chosen_char

                            else:
                                # other type of character so write it to file as it is.
                                temp_word= temp_word +char

                        perturbed_text.append(temp_word)
                        break
                    pos_word_counter=pos_word_counter+1

                if pos_word_found_flag==0:
                     perturbed_text.append(word)
            if word!="\n":
                perturbed_text.append(" ")
    return "".join(perturbed_text)




"""This is the naive method of attack for negative sentiment words: replacing all letters found in text."""
def naive_perturbation_neg(final_map,review):
    perturbed_text = []
    review = review.split("\n")

    for line in review:
        # split line in words
        words = line.split(" ")
        for word in words:
            temp_word = ""
            for char in word:
                if char>='A' and char<='Z':
                    #find the sublist pointer from the final_map list.
                    pointer = ord(char) - ord('A')
                    #get random char from the final_map to replace the original char.
                    random_choice = random.randrange(0, len(final_map[pointer]))

                    # get chosen hex num
                    chosen_char = str(final_map[pointer][random_choice].strip(" "))

                    # append 0x to hex num string to convert it.
                    chosen_char = "0x" + chosen_char
                    # get the actual integer of the specific hex num with base 16.
                    chosen_char = int(chosen_char, base=16)
                    # finally get the actual character stored for specific hex char representation.
                    chosen_char = chr(chosen_char)

                    # write modified char to perturbed file.
                    temp_word = temp_word + chosen_char


                elif char>='a' and char<='z':
                    # find the sublist pointer from the final_map list.
                    pointer = ord(char) - ord('a')
                    # get random char from the final_map to replace the original char.
                    random_choice = random.randrange(0, len(final_map[pointer]))

                    #get chosen hex num
                    chosen_char = str(final_map[pointer][random_choice].strip(" "))

                    #append 0x to hex num string to convert it.
                    chosen_char="0x"+chosen_char
                    #get the actual integer of the specific hex num with base 16.
                    chosen_char = int(chosen_char, base=16)
                    #finally get the actual character stored for specific hex char representation.
                    chosen_char = chr(chosen_char)

                    #write modified char to perturbed file.
                    temp_word = temp_word + chosen_char

                else:
                    #other type of character so write it to file as it is.
                    temp_word = temp_word + char
            perturbed_text.append(temp_word)
    return " ".join(perturbed_text)


"""This is the naive method of attack for positive sentiment words: replacing all letters found in text."""
def naive_perturbation_pos(final_map,review):

    perturbed_text = []
    review = review.split("\n")

    for line in review:
        # split line in words
        words = line.split(" ")
        for word in words:
            temp_word = ""
            for char in word:
                if char>='A' and char<='Z':
                    #find the sublist pointer from the final_map list.
                    pointer = ord(char) - ord('A')
                    #get random char from the final_map to replace the original char.
                    random_choice = random.randrange(0, len(final_map[pointer]))

                    # get chosen hex num
                    chosen_char = str(final_map[pointer][random_choice].strip(" "))

                    # append 0x to hex num string to convert it.
                    chosen_char = "0x" + chosen_char
                    # get the actual integer of the specific hex num with base 16.
                    chosen_char = int(chosen_char, base=16)
                    # finally get the actual character stored for specific hex char representation.
                    chosen_char = chr(chosen_char)

                    # write modified char to perturbed file.
                    temp_word = temp_word + chosen_char

                elif char>='a' and char<='z':
                    # find the sublist pointer from the final_map list.
                    pointer = ord(char) - ord('a')
                    # get random char from the final_map to replace the original char.
                    random_choice = random.randrange(0, len(final_map[pointer]))

                    #get chosen hex num
                    chosen_char = str(final_map[pointer][random_choice].strip(" "))

                    #append 0x to hex num string to convert it.
                    chosen_char="0x"+chosen_char
                    #get the actual integer of the specific hex num with base 16.
                    chosen_char = int(chosen_char, base=16)
                    #finally get the actual character stored for specific hex char representation.
                    chosen_char = chr(chosen_char)

                    #write modified char to perturbed file.
                    temp_word = temp_word + chosen_char

                else:
                    #other type of character so write it to file as it is.
                    temp_word = temp_word + char

            perturbed_text.append(temp_word)

    return " ".join(perturbed_text)



"""This is an attack where we double the positive sentiment words found in positive-words list."""
def double_pos_words(final_map,review):
    #initialize Porter Stemmer
    ps = PorterStemmer()

    # read the positive sentiment words in opinion_words/negative_sentiment_words/negative-words.txt
    with open("opinion_words/positive_sentiment_words/positive-words.txt") as f:
        positive_sentiment_words = f.readlines()
    neg_local = []
    for word in positive_sentiment_words:
        neg_local.append(word.strip("\n"))

    positive_sentiment_words=neg_local

    # read the stemmed negative sentiment words in opinion_words/negative_sentiment_words/stemmed-negative-words.txt
    with open("opinion_words/positive_sentiment_words/stemmed-positive-words.txt") as fstemmed:
        positive_sentiment_words_stemmed = fstemmed.readlines()
    neg_local_stemmed = []
    for word in positive_sentiment_words_stemmed:
        neg_local_stemmed.append(word.strip("\n"))

    positive_sentiment_words_stemmed=neg_local_stemmed

    perturbed_text = []
    review = review.split("\n")

    for line in review:
        #split line in words
        words = line.split(" ")
        for word in words:
            pos_word_found_flag=0
            if len(word)==1:
                perturbed_text.append(word)
            else:
                pos_word_counter=0
                for pos_word in positive_sentiment_words:
                    #if pos_word==word:
                    #if neg_word==word or ps.stem(neg_word)==ps.stem(word):
                    if pos_word == word or positive_sentiment_words_stemmed[pos_word_counter] == ps.stem(word):
                        #print(neg_word+" ==   "+word)
                        #print(ps.stem(neg_word)+"  "+ps.stem(word))
                        pos_word_found_flag=1

                        #double the pos word found
                        perturbed_text.append(word)
                        perturbed_text.append(word)
                        #pos word found so break
                        break

                    pos_word_counter=pos_word_counter+1

                if pos_word_found_flag==0:
                     perturbed_text.append(word)
            if word!="\n":
                perturbed_text.append(" ")
    return "".join(perturbed_text)


"""This is an attack where we double the negative sentiment words found in negative-words list."""
def double_neg_words(final_map,review):
    #initialize Porter Stemmer
    ps = PorterStemmer()

    # read the negative sentiment words in opinion_words/negative_sentiment_words/negative-words.txt
    with open("opinion_words/negative_sentiment_words/negative-words.txt") as f:
        negative_sentiment_words = f.readlines()
    neg_local = []
    for word in negative_sentiment_words:
        neg_local.append(word.strip("\n"))

    negative_sentiment_words=neg_local

    # read the stemmed negative sentiment words in opinion_words/negative_sentiment_words/stemmed-negative-words.txt
    with open("opinion_words/negative_sentiment_words/stemmed-negative-words.txt") as fstemmed:
        negative_sentiment_words_stemmed = fstemmed.readlines()
    neg_local_stemmed = []
    for word in negative_sentiment_words_stemmed:
        neg_local_stemmed.append(word.strip("\n"))

    negative_sentiment_words_stemmed=neg_local_stemmed

    perturbed_text = []
    review = review.split("\n")

    for line in review:
        #split line in words
        words = line.split(" ")
        for word in words:
            neg_word_found_flag=0
            if len(word)==1:
                perturbed_text.append(word)
            else:
                neg_word_counter=0
                for neg_word in negative_sentiment_words:
                    #if neg_word==word:
                    #if neg_word==word or ps.stem(neg_word)==ps.stem(word):
                    if neg_word == word or negative_sentiment_words_stemmed[neg_word_counter] == ps.stem(word):
                        #print(neg_word+" ==   "+word)
                        #print(ps.stem(neg_word)+"  "+ps.stem(word))
                        neg_word_found_flag=1

                        # double the pos word found
                        perturbed_text.append(word)
                        perturbed_text.append(word)
                        # pos word found so break
                        break

                    neg_word_counter=neg_word_counter+1

                if neg_word_found_flag==0:
                     perturbed_text.append(word)
            if word!="\n":
                perturbed_text.append(" ")
    return "".join(perturbed_text)


"""This is an attack where we double the positive sentiment words found in positive-words list and also perturb them."""
def double_pos_words_perturbed(final_map,review):
    #initialize Porter Stemmer
    ps = PorterStemmer()

    # read the positive sentiment words in opinion_words/negative_sentiment_words/negative-words.txt
    with open("opinion_words/positive_sentiment_words/positive-words.txt") as f:
        positive_sentiment_words = f.readlines()
    neg_local = []
    for word in positive_sentiment_words:
        neg_local.append(word.strip("\n"))

    positive_sentiment_words=neg_local

    # read the stemmed negative sentiment words in opinion_words/negative_sentiment_words/stemmed-negative-words.txt
    with open("opinion_words/positive_sentiment_words/stemmed-positive-words.txt") as fstemmed:
        positive_sentiment_words_stemmed = fstemmed.readlines()
    neg_local_stemmed = []
    for word in positive_sentiment_words_stemmed:
        neg_local_stemmed.append(word.strip("\n"))

    positive_sentiment_words_stemmed=neg_local_stemmed

    perturbed_text = []
    review = review.split("\n")

    for line in review:
        #split line in words
        words = line.split(" ")
        for word in words:
            pos_word_found_flag=0
            if len(word)==1:
                perturbed_text.append(word)
            else:
                pos_word_counter=0
                for pos_word in positive_sentiment_words:
                    #if pos_word==word:
                    #if neg_word==word or ps.stem(neg_word)==ps.stem(word):
                    if pos_word == word or positive_sentiment_words_stemmed[pos_word_counter] == ps.stem(word):
                        #print(neg_word+" ==   "+word)
                        #print(ps.stem(neg_word)+"  "+ps.stem(word))
                        pos_word_found_flag=1

                        # change parameter to double=2 triple=3 etc
                        multiply_by = 2
                        for c in range(0, multiply_by):

                            temp_word = ""
                            #negative sentiment word found so perturb it and write it to file
                            for char in word:
                                if char >= 'A' and char <= 'Z':
                                    # find the sublist pointer from the final_map list.
                                    pointer = ord(char) - ord('A')
                                    # get random char from the final_map to replace the original char.
                                    random_choice = random.randrange(0, len(final_map[pointer]))

                                    # get chosen hex num
                                    chosen_char = str(final_map[pointer][random_choice].strip(" "))

                                    # append 0x to hex num string to convert it.
                                    chosen_char = "0x" + chosen_char
                                    # get the actual integer of the specific hex num with base 16.
                                    chosen_char = int(chosen_char, base=16)
                                    # finally get the actual character stored for specific hex char representation.
                                    chosen_char = chr(chosen_char)

                                    # write modified char to perturbed file.
                                    temp_word = temp_word + chosen_char

                                elif char >= 'a' and char <= 'z':
                                    # find the sublist pointer from the final_map list.
                                    pointer = ord(char) - ord('a')
                                    # get random char from the final_map to replace the original char.
                                    random_choice = random.randrange(0, len(final_map[pointer]))

                                    # get chosen hex num
                                    chosen_char = str(final_map[pointer][random_choice].strip(" "))

                                    # append 0x to hex num string to convert it.
                                    chosen_char = "0x" + chosen_char
                                    # get the actual integer of the specific hex num with base 16.
                                    chosen_char = int(chosen_char, base=16)
                                    # finally get the actual character stored for specific hex char representation.
                                    chosen_char = chr(chosen_char)

                                    # write modified char to perturbed file.
                                    temp_word = temp_word + chosen_char

                                else:
                                    # other type of character so write it to file as it is.
                                    temp_word = temp_word + char

                        temp_word= temp_word + temp_word
                        perturbed_text.append(temp_word)
                        break

                    pos_word_counter=pos_word_counter+1

                if pos_word_found_flag==0:
                     perturbed_text.append(word)
            if word!="\n":
                perturbed_text.append(" ")
    return "".join(perturbed_text)


"""This is an attack where we double the negative sentiment words found in negative-words list and perturbed."""
def double_neg_words_perturbed(final_map,review):
    #initialize Porter Stemmer
    ps = PorterStemmer()

    # read the negative sentiment words in opinion_words/negative_sentiment_words/negative-words.txt
    with open("opinion_words/negative_sentiment_words/negative-words.txt") as f:
        negative_sentiment_words = f.readlines()
    neg_local = []
    for word in negative_sentiment_words:
        neg_local.append(word.strip("\n"))

    negative_sentiment_words=neg_local

    # read the stemmed negative sentiment words in opinion_words/negative_sentiment_words/stemmed-negative-words.txt
    with open("opinion_words/negative_sentiment_words/stemmed-negative-words.txt") as fstemmed:
        negative_sentiment_words_stemmed = fstemmed.readlines()
    neg_local_stemmed = []
    for word in negative_sentiment_words_stemmed:
        neg_local_stemmed.append(word.strip("\n"))

    negative_sentiment_words_stemmed=neg_local_stemmed


    perturbed_text = []
    review = review.split("\n")


    for line in review:
        #split line in words
        words = line.split(" ")
        for word in words:
            neg_word_found_flag=0
            if len(word)==1:
                perturbed_text.append(word)
            else:
                neg_word_counter=0
                for neg_word in negative_sentiment_words:
                    #if neg_word==word:
                    #if neg_word==word or ps.stem(neg_word)==ps.stem(word):
                    if neg_word == word or negative_sentiment_words_stemmed[neg_word_counter] == ps.stem(word):
                        #print(neg_word+" ==   "+word)
                        #print(ps.stem(neg_word)+"  "+ps.stem(word))
                        neg_word_found_flag=1

                        # change parameter to double=2 triple=3 etc
                        multiply_by = 2
                        for c in range(0, multiply_by):

                            temp_word = ""
                            #negative sentiment word found so perturb it and write it to file
                            for char in word:
                                if char >= 'A' and char <= 'Z':
                                    # find the sublist pointer from the final_map list.
                                    pointer = ord(char) - ord('A')
                                    # get random char from the final_map to replace the original char.
                                    random_choice = random.randrange(0, len(final_map[pointer]))

                                    # get chosen hex num
                                    chosen_char = str(final_map[pointer][random_choice].strip(" "))

                                    # append 0x to hex num string to convert it.
                                    chosen_char = "0x" + chosen_char
                                    # get the actual integer of the specific hex num with base 16.
                                    chosen_char = int(chosen_char, base=16)
                                    # finally get the actual character stored for specific hex char representation.
                                    chosen_char = chr(chosen_char)

                                    # write modified char to perturbed file.
                                    temp_word = temp_word +chosen_char

                                elif char >= 'a' and char <= 'z':
                                    # find the sublist pointer from the final_map list.
                                    pointer = ord(char) - ord('a')
                                    # get random char from the final_map to replace the original char.
                                    random_choice = random.randrange(0, len(final_map[pointer]))

                                    # get chosen hex num
                                    chosen_char = str(final_map[pointer][random_choice].strip(" "))

                                    # append 0x to hex num string to convert it.
                                    chosen_char = "0x" + chosen_char
                                    # get the actual integer of the specific hex num with base 16.
                                    chosen_char = int(chosen_char, base=16)
                                    # finally get the actual character stored for specific hex char representation.
                                    chosen_char = chr(chosen_char)

                                    # write modified char to perturbed file.
                                    temp_word = temp_word +chosen_char

                                else:
                                    # other type of character so write it to file as it is.
                                    temp_word = temp_word +char

                        temp_word= temp_word + temp_word
                        perturbed_text.append(temp_word)
                        # neg word found so break
                        break

                    neg_word_counter=neg_word_counter+1

                if neg_word_found_flag==0:
                     perturbed_text.append(word)
            if word!="\n":
                perturbed_text.append(" ")
    return "".join(perturbed_text)

"""This is an attack where we insert space between each letter in negative sentiment words found in negative-words list."""
def insert_space_neg(final_map,review):
    #initialize Porter Stemmer
    ps = PorterStemmer()

    # read the negative sentiment words in opinion_words/negative_sentiment_words/negative-words.txt
    with open("opinion_words/negative_sentiment_words/negative-words.txt") as f:
        negative_sentiment_words = f.readlines()
    neg_local = []
    for word in negative_sentiment_words:
        neg_local.append(word.strip("\n"))

    negative_sentiment_words=neg_local

    # read the stemmed negative sentiment words in opinion_words/negative_sentiment_words/stemmed-negative-words.txt
    with open("opinion_words/negative_sentiment_words/stemmed-negative-words.txt") as fstemmed:
        negative_sentiment_words_stemmed = fstemmed.readlines()
    neg_local_stemmed = []
    for word in negative_sentiment_words_stemmed:
        neg_local_stemmed.append(word.strip("\n"))

    negative_sentiment_words_stemmed=neg_local_stemmed

    perturbed_text = []
    review = review.split("\n")


    for line in review:
        #split line in words
        words = line.split(" ")
        for word in words:
            neg_word_found_flag=0
            if len(word)==1:
                perturbed_text.append(word)
            else:
                neg_word_counter=0
                for neg_word in negative_sentiment_words:
                    #if neg_word==word:
                    #if neg_word==word or ps.stem(neg_word)==ps.stem(word):
                    if neg_word == word or negative_sentiment_words_stemmed[neg_word_counter] == ps.stem(word):
                        #print(neg_word+" ==   "+word)
                        #print(ps.stem(neg_word)+"  "+ps.stem(word))
                        neg_word_found_flag=1

                        temp_word=""
                        for lett in word:
                            temp_word = temp_word + lett
                            temp_word = temp_word + " "

                        perturbed_text.append(temp_word)
                        # pos word found so break
                        break

                    neg_word_counter=neg_word_counter+1

                if neg_word_found_flag==0:
                     perturbed_text.append(word)
            if word!="\n":
                perturbed_text.append(" ")
    return "".join(perturbed_text)

"""This is an attack where we insert space between each letter in positive sentiment words found in positive-words list."""
def insert_space_pos(final_map,review):
    # initialize Porter Stemmer
    ps = PorterStemmer()

    # read the positive sentiment words in opinion_words/negative_sentiment_words/negative-words.txt
    with open("opinion_words/positive_sentiment_words/positive-words.txt") as f:
        positive_sentiment_words = f.readlines()
    neg_local = []
    for word in positive_sentiment_words:
        neg_local.append(word.strip("\n"))

    positive_sentiment_words = neg_local

    # read the stemmed negative sentiment words in opinion_words/negative_sentiment_words/stemmed-negative-words.txt
    with open("opinion_words/positive_sentiment_words/stemmed-positive-words.txt") as fstemmed:
        positive_sentiment_words_stemmed = fstemmed.readlines()
    neg_local_stemmed = []
    for word in positive_sentiment_words_stemmed:
        neg_local_stemmed.append(word.strip("\n"))

    positive_sentiment_words_stemmed = neg_local_stemmed

    perturbed_text = []
    review = review.split("\n")

    for line in review:
        # split line in words
        words = line.split(" ")
        for word in words:
            pos_word_found_flag = 0
            if len(word) == 1:
                perturbed_text.append(word)
            else:
                pos_word_counter = 0
                for pos_word in positive_sentiment_words:
                    #if pos_word == word:
                    # if neg_word==word or ps.stem(neg_word)==ps.stem(word):
                    if pos_word == word or positive_sentiment_words_stemmed[pos_word_counter] == ps.stem(word):
                        # print(neg_word+" ==   "+word)
                        # print(ps.stem(neg_word)+"  "+ps.stem(word))
                        pos_word_found_flag = 1
                        temp_word = ""
                        for lett in word:
                            temp_word = temp_word +lett
                            temp_word = temp_word + " "

                        perturbed_text.append(temp_word)
                        # pos word found so break
                        break

                    pos_word_counter = pos_word_counter + 1

                if pos_word_found_flag == 0:
                    perturbed_text.append(word)
            if word != "\n":
                perturbed_text.append(" ")
    return "".join(perturbed_text)


"""This is an attack where we insert space between characters in the negative sentiment words found in negative-words list and perturb them."""
def insert_space_and_perturbed_neg(final_map,review):
    #initialize Porter Stemmer
    ps = PorterStemmer()

    # read the negative sentiment words in opinion_words/negative_sentiment_words/negative-words.txt
    with open("opinion_words/negative_sentiment_words/negative-words.txt") as f:
        negative_sentiment_words = f.readlines()
    neg_local = []
    for word in negative_sentiment_words:
        neg_local.append(word.strip("\n"))

    negative_sentiment_words=neg_local

    # read the stemmed negative sentiment words in opinion_words/negative_sentiment_words/stemmed-negative-words.txt
    with open("opinion_words/negative_sentiment_words/stemmed-negative-words.txt") as fstemmed:
        negative_sentiment_words_stemmed = fstemmed.readlines()
    neg_local_stemmed = []
    for word in negative_sentiment_words_stemmed:
        neg_local_stemmed.append(word.strip("\n"))

    negative_sentiment_words_stemmed=neg_local_stemmed

    perturbed_text = []
    review = review.split("\n")

    for line in review:
        #split line in words
        words = line.split(" ")
        for word in words:
            neg_word_found_flag=0
            if len(word)==1:
                perturbed_text.append(word)
            else:
                neg_word_counter=0
                for neg_word in negative_sentiment_words:
                    #if neg_word==word:
                    #if neg_word==word or ps.stem(neg_word)==ps.stem(word):
                    if neg_word == word or negative_sentiment_words_stemmed[neg_word_counter] == ps.stem(word):
                        #print(neg_word+" ==   "+word)
                        #print(ps.stem(neg_word)+"  "+ps.stem(word))
                        neg_word_found_flag=1
                        #negative sentiment word found so perturb it and write it to file
                        temp_word=""
                        for char in word:
                            if char >= 'A' and char <= 'Z':
                                # find the sublist pointer from the final_map list.
                                pointer = ord(char) - ord('A')
                                # get random char from the final_map to replace the original char.
                                random_choice = random.randrange(0, len(final_map[pointer]))

                                # get chosen hex num
                                chosen_char = str(final_map[pointer][random_choice].strip(" "))

                                # append 0x to hex num string to convert it.
                                chosen_char = "0x" + chosen_char
                                # get the actual integer of the specific hex num with base 16.
                                chosen_char = int(chosen_char, base=16)
                                # finally get the actual character stored for specific hex char representation.
                                chosen_char = chr(chosen_char)

                                # write modified char to perturbed file.
                                temp_word = temp_word + chosen_char
                                temp_word = temp_word + " "

                            elif char >= 'a' and char <= 'z':
                                # find the sublist pointer from the final_map list.
                                pointer = ord(char) - ord('a')
                                # get random char from the final_map to replace the original char.
                                random_choice = random.randrange(0, len(final_map[pointer]))

                                # get chosen hex num
                                chosen_char = str(final_map[pointer][random_choice].strip(" "))

                                # append 0x to hex num string to convert it.
                                chosen_char = "0x" + chosen_char
                                # get the actual integer of the specific hex num with base 16.
                                chosen_char = int(chosen_char, base=16)
                                # finally get the actual character stored for specific hex char representation.
                                chosen_char = chr(chosen_char)

                                # write modified char to perturbed file.
                                temp_word = temp_word + chosen_char
                                temp_word = temp_word + " "

                            else:
                                # other type of character so write it to file as it is.
                                temp_word = temp_word + char
                                temp_word = temp_word + " "

                        perturbed_text.append(temp_word)
                        break
                    neg_word_counter=neg_word_counter+1

                if neg_word_found_flag==0:
                     perturbed_text.append(word)
            if word!="\n":
                perturbed_text.append(" ")
    return "".join(perturbed_text)



"""This is an attack where we insert space between characters in the positive sentiment words found in positive-words list and perturbed them."""
def insert_space_and_perturbed_pos(final_map,review):
    #initialize Porter Stemmer
    ps = PorterStemmer()

    # read the negative sentiment words in opinion_words/negative_sentiment_words/negative-words.txt
    with open("opinion_words/positive_sentiment_words/positive-words.txt") as f:
        positive_sentiment_words = f.readlines()
    neg_local = []
    for word in positive_sentiment_words:
        neg_local.append(word.strip("\n"))

    positive_sentiment_words=neg_local

    # read the stemmed negative sentiment words in opinion_words/negative_sentiment_words/stemmed-negative-words.txt
    with open("opinion_words/positive_sentiment_words/stemmed-positive-words.txt") as fstemmed:
        positive_sentiment_words_stemmed = fstemmed.readlines()
    neg_local_stemmed = []
    for word in positive_sentiment_words_stemmed:
        neg_local_stemmed.append(word.strip("\n"))

    positive_sentiment_words_stemmed=neg_local_stemmed

    perturbed_text = []
    review = review.split("\n")

    for line in review:
        #split line in words
        words = line.split(" ")
        for word in words:
            pos_word_found_flag=0
            if len(word)==1:
                perturbed_text.append(word)
            else:
                pos_word_counter=0
                for pos_word in positive_sentiment_words:

                    if pos_word == word or positive_sentiment_words_stemmed[pos_word_counter] == ps.stem(word):
                        #print(neg_word+" ==   "+word)
                        #print(ps.stem(neg_word)+"  "+ps.stem(word))
                        pos_word_found_flag=1
                        #negative sentiment word found so perturb it and write it to file
                        temp_word=""
                        for char in word:
                            if char >= 'A' and char <= 'Z':
                                # find the sublist pointer from the final_map list.
                                pointer = ord(char) - ord('A')
                                # get random char from the final_map to replace the original char.
                                random_choice = random.randrange(0, len(final_map[pointer]))

                                # get chosen hex num
                                chosen_char = str(final_map[pointer][random_choice].strip(" "))

                                # append 0x to hex num string to convert it.
                                chosen_char = "0x" + chosen_char
                                # get the actual integer of the specific hex num with base 16.
                                chosen_char = int(chosen_char, base=16)
                                # finally get the actual character stored for specific hex char representation.
                                chosen_char = chr(chosen_char)

                                # write modified char to perturbed file.
                                temp_word = temp_word+chosen_char
                                temp_word = temp_word+" "

                            elif char >= 'a' and char <= 'z':
                                # find the sublist pointer from the final_map list.
                                pointer = ord(char) - ord('a')
                                # get random char from the final_map to replace the original char.
                                random_choice = random.randrange(0, len(final_map[pointer]))

                                # get chosen hex num
                                chosen_char = str(final_map[pointer][random_choice].strip(" "))

                                # append 0x to hex num string to convert it.
                                chosen_char = "0x" + chosen_char
                                # get the actual integer of the specific hex num with base 16.
                                chosen_char = int(chosen_char, base=16)
                                # finally get the actual character stored for specific hex char representation.
                                chosen_char = chr(chosen_char)

                                # write modified char to perturbed file.
                                temp_word = temp_word+chosen_char
                                temp_word = temp_word+" "

                            else:
                                # other type of character so write it to file as it is.
                                temp_word = temp_word+char
                                temp_word = temp_word+" "

                        perturbed_text.append(temp_word)
                        break
                    pos_word_counter=pos_word_counter+1

                if pos_word_found_flag==0:
                     perturbed_text.append(word)
            if word!="\n":
                perturbed_text.append(" ")
    return "".join(perturbed_text)

"""This is an attack where we capitalize random letters in a negative sentiment word."""
def random_capitals_neg(final_map,review):
    #initialize Porter Stemmer
    ps = PorterStemmer()

    # read the negative sentiment words in opinion_words/negative_sentiment_words/negative-words.txt
    with open("opinion_words/negative_sentiment_words/negative-words.txt") as f:
        negative_sentiment_words = f.readlines()
    neg_local = []
    for word in negative_sentiment_words:
        neg_local.append(word.strip("\n"))

    negative_sentiment_words=neg_local

    # read the stemmed negative sentiment words in opinion_words/negative_sentiment_words/stemmed-negative-words.txt
    with open("opinion_words/negative_sentiment_words/stemmed-negative-words.txt") as fstemmed:
        negative_sentiment_words_stemmed = fstemmed.readlines()
    neg_local_stemmed = []
    for word in negative_sentiment_words_stemmed:
        neg_local_stemmed.append(word.strip("\n"))

    negative_sentiment_words_stemmed=neg_local_stemmed

    perturbed_text = []
    review = review.split("\n")


    for line in review:
        #split line in words
        words = line.split(" ")
        for word in words:
            neg_word_found_flag=0
            if len(word)==1:
                perturbed_text.append(word)
            else:
                neg_word_counter=0
                for neg_word in negative_sentiment_words:

                    if neg_word == word or negative_sentiment_words_stemmed[neg_word_counter] == ps.stem(word):
                        #print(neg_word+" ==   "+word)
                        #print(ps.stem(neg_word)+"  "+ps.stem(word))
                        neg_word_found_flag=1

                        temp_word= ""
                        for lett in word:
                            if (lett >= 'A' and lett <= 'Z') or (lett >= 'a' and lett <= 'z'):
                                # calculate a random num between 0-1. if 0 then small if 1 then caps.
                                caps_or_small = random.randrange(0, 2)
                                if caps_or_small == 0:
                                    lett = lett.lower()
                                    temp_word = temp_word + lett
                                elif caps_or_small == 1:
                                    lett = lett.lower()
                                    lett = lett.upper()
                                    temp_word = temp_word + lett
                            else:
                                # not a letter to write it as it is.
                                temp_word = temp_word + lett

                        perturbed_text.append(temp_word)
                        # pos word found so break
                        break

                    neg_word_counter=neg_word_counter+1

                if neg_word_found_flag==0:
                     perturbed_text.append(word)
            if word!="\n":
                perturbed_text.append(" ")
    return "".join(perturbed_text)

"""This is an attack where we capitalize random letters in a positive sentiment word."""
def random_capitals_pos(final_map,review):
    # initialize Porter Stemmer
    ps = PorterStemmer()

    # read the positive sentiment words in opinion_words/negative_sentiment_words/negative-words.txt
    with open("opinion_words/positive_sentiment_words/positive-words.txt") as f:
        positive_sentiment_words = f.readlines()
    neg_local = []
    for word in positive_sentiment_words:
        neg_local.append(word.strip("\n"))

    positive_sentiment_words = neg_local

    # read the stemmed negative sentiment words in opinion_words/negative_sentiment_words/stemmed-negative-words.txt
    with open("opinion_words/positive_sentiment_words/stemmed-positive-words.txt") as fstemmed:
        positive_sentiment_words_stemmed = fstemmed.readlines()
    neg_local_stemmed = []
    for word in positive_sentiment_words_stemmed:
        neg_local_stemmed.append(word.strip("\n"))

    positive_sentiment_words_stemmed = neg_local_stemmed

    perturbed_text = []

    review = review.split("\n")

    for line in review:
        # split line in words
        words = line.split(" ")
        for word in words:
            pos_word_found_flag = 0
            if len(word) == 1:
                perturbed_text.append(word)
            else:
                pos_word_counter = 0
                for pos_word in positive_sentiment_words:
                    if pos_word == word or positive_sentiment_words_stemmed[pos_word_counter] == ps.stem(word):
                        pos_word_found_flag = 1

                        temp_word = ""
                        for lett in word:
                            if (lett>='A' and lett<='Z') or (lett>='a' and lett<='z'):
                                # calculate a random num between 0-1. if 0 then small if 1 then caps.
                                caps_or_small = random.randrange(0, 2)
                                if caps_or_small == 0:
                                    lett = lett.lower()
                                    temp_word = temp_word + lett
                                elif caps_or_small == 1:
                                    lett = lett.lower()
                                    lett = lett.upper()
                                    temp_word = temp_word + lett
                            else:
                                #not a letter to write it as it is.
                                temp_word = temp_word + lett

                        perturbed_text.append(temp_word)
                        # pos word found so break
                        break

                    pos_word_counter = pos_word_counter + 1

                if pos_word_found_flag == 0:
                    perturbed_text.append(word)
            if word != "\n":
                perturbed_text.append(" ")


    return "".join(perturbed_text)


"""This is an attack where we lowercase the 1st letter and capitalize the second in negative sentiment words."""
def first_lowercase_only_neg(final_map,review):
    #initialize Porter Stemmer
    ps = PorterStemmer()

    # read the negative sentiment words in opinion_words/negative_sentiment_words/negative-words.txt
    with open("opinion_words/negative_sentiment_words/negative-words.txt") as f:
        negative_sentiment_words = f.readlines()
    neg_local = []
    for word in negative_sentiment_words:
        neg_local.append(word.strip("\n"))

    negative_sentiment_words=neg_local

    # read the stemmed negative sentiment words in opinion_words/negative_sentiment_words/stemmed-negative-words.txt
    with open("opinion_words/negative_sentiment_words/stemmed-negative-words.txt") as fstemmed:
        negative_sentiment_words_stemmed = fstemmed.readlines()
    neg_local_stemmed = []
    for word in negative_sentiment_words_stemmed:
        neg_local_stemmed.append(word.strip("\n"))

    negative_sentiment_words_stemmed=neg_local_stemmed

    perturbed_text = []

    review = review.split("\n")

    for line in review:
        #split line in words
        words = line.split(" ")
        for word in words:
            neg_word_found_flag=0
            if len(word)==1:
                perturbed_text.append(word)
            else:
                neg_word_counter=0
                for neg_word in negative_sentiment_words:
                    if neg_word == word or negative_sentiment_words_stemmed[neg_word_counter] == ps.stem(word):
                        neg_word_found_flag=1

                        # make 1st letter lowercase
                        first_lett = neg_word[0].lower()
                        temp_word=first_lett

                        # make other letters uppercase
                        for cou in range(1, len(word)):
                            lett = word[cou].upper()
                            temp_word = temp_word + lett

                        perturbed_text.append(temp_word)
                        # pos word found so break
                        break

                    neg_word_counter=neg_word_counter+1

                if neg_word_found_flag==0:
                    perturbed_text.append(word)
            if word!="\n":
                perturbed_text.append(" ")
    return "".join(perturbed_text)

"""This is an attack where we lowercase the 1st letter and capitalize the second in positive sentiment words."""
def first_lowercase_only_pos(final_map,review):
    # initialize Porter Stemmer
    ps = PorterStemmer()

    # read the positive sentiment words in opinion_words/negative_sentiment_words/negative-words.txt
    with open("opinion_words/positive_sentiment_words/positive-words.txt") as f:
        positive_sentiment_words = f.readlines()
    neg_local = []
    for word in positive_sentiment_words:
        neg_local.append(word.strip("\n"))

    positive_sentiment_words = neg_local

    # read the stemmed negative sentiment words in opinion_words/negative_sentiment_words/stemmed-negative-words.txt
    with open("opinion_words/positive_sentiment_words/stemmed-positive-words.txt") as fstemmed:
        positive_sentiment_words_stemmed = fstemmed.readlines()
    neg_local_stemmed = []
    for word in positive_sentiment_words_stemmed:
        neg_local_stemmed.append(word.strip("\n"))

    positive_sentiment_words_stemmed = neg_local_stemmed

    perturbed_text = []
    review = review.split("\n")

    for line in review:
        # split line in words
        words = line.split(" ")
        for word in words:
            pos_word_found_flag = 0
            if len(word) == 1:
                perturbed_text.append(word)
            else:
                pos_word_counter = 0
                for pos_word in positive_sentiment_words:
                    temp_word = ""
                    if pos_word == word or positive_sentiment_words_stemmed[pos_word_counter] == ps.stem(word):
                        # print(neg_word+" ==   "+word)
                        # print(ps.stem(neg_word)+"  "+ps.stem(word))
                        pos_word_found_flag = 1

                        #make 1st letter lowercase
                        first_lett = pos_word[0].lower()
                        temp_word=first_lett

                        #make other letters uppercase
                        for cou in range(1,len(word)):
                            lett = word[cou].upper()
                            temp_word = temp_word + lett

                        perturbed_text.append(temp_word)
                        # pos word found so break
                        break

                    pos_word_counter = pos_word_counter + 1

                if pos_word_found_flag == 0:
                    perturbed_text.append(word)
            if word != "\n":
                perturbed_text.append(" ")
    return "".join(perturbed_text)



"""This is an attack where we replace small length positive sentiment words with bigger ones in order to perturb them easily."""
def replace_small_length_words_pos(final_map,review):

    # initialize Porter Stemmer
    ps = PorterStemmer()

    # read pos words in pos_words list
    with open("offensive_words/final_similar_words_pos.txt") as f:
        lines = f.readlines()
        pos_words = []
        for line in lines:
            line = line.split(":")
            pos_words.append(line[0])

    positive_sentiment_words = pos_words

    # read top-5 similar words in similar_pos_words list
    similar_pos_words = []

    for line in lines:
        line = line.split(":")[1]
        line = line.split(" ")
        local = []
        for word in line:
            # skip blanks, \n, etc.
            if len(word) > 1:
                local.append(word)

        # add top-5 similar words to similar_neg_words list
        if len(local) != 5:
            print("ERROR top similar words are not 5!!!")
            exit(0)
        similar_pos_words.append(local)

    perturbed_text = []

    review = review.split("\n")

    for line in review:
        # split line in words
        words = line.split(" ")
        for word in words:
            pos_word_found_flag = 0
            if len(word) == 1:
                perturbed_text.append(word)
            else:
                pos_word_counter = 0
                for pos_word in positive_sentiment_words:
                    if pos_word == word or ps.stem(pos_word) == ps.stem(word):
                        pos_word_found_flag = 1

                        # replace with a larger length neg word from similar_neg_words list.
                        # 1st calculate a random num to choose neg word replacement
                        counter = 0
                        while counter < 5:
                            replacement_word_index = random.randrange(0, 5)
                            new_word = similar_pos_words[pos_word_counter][replacement_word_index]
                            if len(new_word) > len(word):
                                word = new_word
                                break
                            counter = counter + 1

                        # negative sentiment word found so perturb it and write it to file
                        temp_word=""
                        for char in word:
                            if char >= 'A' and char <= 'Z':
                                # find the sublist pointer from the final_map list.
                                pointer = ord(char) - ord('A')
                                # get random char from the final_map to replace the original char.
                                random_choice = random.randrange(0, len(final_map[pointer]))

                                # get chosen hex num
                                chosen_char = str(final_map[pointer][random_choice].strip(" "))

                                # append 0x to hex num string to convert it.
                                chosen_char = "0x" + chosen_char
                                # get the actual integer of the specific hex num with base 16.
                                chosen_char = int(chosen_char, base=16)
                                # finally get the actual character stored for specific hex char representation.
                                chosen_char = chr(chosen_char)

                                # write modified char to perturbed file.
                                temp_word=temp_word+chosen_char

                            elif char >= 'a' and char <= 'z':
                                # find the sublist pointer from the final_map list.
                                pointer = ord(char) - ord('a')
                                # get random char from the final_map to replace the original char.
                                random_choice = random.randrange(0, len(final_map[pointer]))

                                # get chosen hex num
                                chosen_char = str(final_map[pointer][random_choice].strip(" "))

                                # append 0x to hex num string to convert it.
                                chosen_char = "0x" + chosen_char
                                # get the actual integer of the specific hex num with base 16.
                                chosen_char = int(chosen_char, base=16)
                                # finally get the actual character stored for specific hex char representation.
                                chosen_char = chr(chosen_char)

                                # write modified char to perturbed file.
                                temp_word=temp_word+chosen_char

                            else:
                                # other type of character so write it to file as it is.
                                temp_word=temp_word+char

                        perturbed_text.append(temp_word)
                        # pos word found so break
                        break

                    pos_word_counter = pos_word_counter + 1

                if pos_word_found_flag == 0:
                    perturbed_text.append(word)
            if word != "\n":
                perturbed_text.append(" ")

    return "".join(perturbed_text)


"""This is an attack where we replace small length negative sentiment words with bigger ones in order to perturb them easily."""
def replace_small_length_words_neg(final_map,review):

    # initialize Porter Stemmer
    ps = PorterStemmer()

    #read neg words in neg_words list
    with open("offensive_words/final_similar_words_neg.txt") as f:
        lines = f.readlines()
        neg_words = []
        for line in lines:
            line = line.split(":")
            neg_words.append(line[0])

    negative_sentiment_words=neg_words

    #read top-5 similar words in similar_neg_words list
    similar_neg_words=[]
    for line in lines:
        line = line.split(":")[1]
        line=line.split(" ")
        local = []
        for word in line:
            #skip blanks, \n, etc.
            if len(word)>1:
                local.append(word)

        #add top-5 similar words to similar_neg_words list
        if len(local)!=5:
            print("ERROR top similar words are not 5!!!")
            exit(0)
        similar_neg_words.append(local)

    perturbed_text = []

    review = review.split("\n")
    for line in review:
        #split line in words
        words = line.split(" ")
        for word in words:
            neg_word_found_flag=0
            if len(word)==1:
                perturbed_text.append(word)
            else:
                neg_word_counter=0
                for neg_word in negative_sentiment_words:

                    if neg_word == word or ps.stem(neg_word) == ps.stem(word):

                        neg_word_found_flag=1

                        # replace with a larger length neg word from similar_neg_words list.
                        #1st calculate a random num to choose neg word replacement
                        counter=0
                        while counter<5:
                            replacement_word_index = random.randrange(0, 5)
                            new_word = similar_neg_words[neg_word_counter][replacement_word_index]
                            if len(new_word)>len(word):
                                word = new_word
                                break
                            counter=counter+1

                        # negative sentiment word found so perturb it and write it to file
                        temp_word=""
                        for char in word:
                            if char >= 'A' and char <= 'Z':
                                # find the sublist pointer from the final_map list.
                                pointer = ord(char) - ord('A')
                                # get random char from the final_map to replace the original char.
                                random_choice = random.randrange(0, len(final_map[pointer]))

                                # get chosen hex num
                                chosen_char = str(final_map[pointer][random_choice].strip(" "))

                                # append 0x to hex num string to convert it.
                                chosen_char = "0x" + chosen_char
                                # get the actual integer of the specific hex num with base 16.
                                chosen_char = int(chosen_char, base=16)
                                # finally get the actual character stored for specific hex char representation.
                                chosen_char = chr(chosen_char)

                                # write modified char to perturbed file.
                                temp_word=temp_word+chosen_char

                            elif char >= 'a' and char <= 'z':
                                # find the sublist pointer from the final_map list.
                                pointer = ord(char) - ord('a')
                                # get random char from the final_map to replace the original char.
                                random_choice = random.randrange(0, len(final_map[pointer]))

                                # get chosen hex num
                                chosen_char = str(final_map[pointer][random_choice].strip(" "))

                                # append 0x to hex num string to convert it.
                                chosen_char = "0x" + chosen_char
                                # get the actual integer of the specific hex num with base 16.
                                chosen_char = int(chosen_char, base=16)
                                # finally get the actual character stored for specific hex char representation.
                                chosen_char = chr(chosen_char)

                                # write modified char to perturbed file.
                                temp_word = temp_word + chosen_char

                            else:
                                # other type of character so write it to file as it is.
                                temp_word = temp_word + char

                        perturbed_text.append(temp_word)
                        # pos word found so break
                        break

                    neg_word_counter=neg_word_counter+1

                if neg_word_found_flag==0:
                    perturbed_text.append(word)
            if word!="\n":
                perturbed_text.append(" ")

    return "".join(perturbed_text)

#Main function
def main():

    #read the map
    final_map = read_map()

    print("Welcome to EvilText framework!!! We are here to perturb text right?! Let's get started ^_^ ")
    choice = input("What's your choice of perturbation? \n"
                   "1: Naive perturbation (Replace each character with a visually-similar one). \n"
                   "2: Replace only negative sentiment/toxic or positive sentiment/non-toxic words.\n"
                   "3: Double negative sentiment/toxic or positive sentiment/non-toxic words.\n"
                   "4: Double negative sentiment/toxic or positive sentiment/non-toxic words and perturb them.\n"
                   "5: Insert space between each letter of negative sentiment/toxic or positive sentiment/non-toxic words.\n"
                   "6: Insert space between each letter of negative sentiment/toxic or positive sentiment/non-toxic words and perturb them.\n"
                   "7: Replace small length negative sentiment/toxic or positive sentiment/non-toxic words with other larger similar ones and perturb them.\n")

    # convert choice from string to int
    choice = int(choice)


    if (choice==1):
        legitimate_text = input("Give the legitimate/original text to be perturbed in 1 line:\n")
        perturbed_text = naive_perturbation_neg(final_map, legitimate_text)
        print(perturbed_text)
    else:

        choice2 = input("Perturbation options:\n"
                        "1: Convert negative sentiment/toxic text to positive/non-toxic text.\n"
                        "2: Convert positive sentiment/non-toxic text to negative/toxic text.\n")

        # convert choice from string to int
        choice2 = int(choice2)

        legitimate_text = input("Give the legitimate/original text to be perturbed in 1 line:\n")

        if (choice==2):
            if choice2 ==1:
                perturbed_text=replace_only_bad_sentiment_words_neg(final_map, legitimate_text)
                print(perturbed_text)
            elif choice2 == 2:
                perturbed_text=replace_only_good_sentiment_words_pos(final_map,legitimate_text)
                print(perturbed_text)
            else:
                print("WRONG input given! Please re-run the program and try again!")
        elif (choice==3):
            if choice2 == 1:
                perturbed_text = double_neg_words(final_map, legitimate_text)
                print(perturbed_text)
            elif choice2 == 2:
                perturbed_text=double_pos_words(final_map,legitimate_text)
                print(perturbed_text)
            else:
                print("WRONG input given! Please re-run the program and try again!")
        elif (choice==4):
            if choice2 == 1:
                perturbed_text=double_neg_words_perturbed(final_map,legitimate_text)
                print(perturbed_text)
            elif choice2 == 2:
                perturbed_text=double_pos_words_perturbed(final_map,legitimate_text)
                print(perturbed_text)
            else:
                print("WRONG input given! Please re-run the program and try again!")
        elif (choice==5):
            if choice2 == 1:
                perturbed_text = insert_space_neg(final_map, legitimate_text)
                print(perturbed_text)
            elif choice2 == 2:
                perturbed_text=insert_space_pos(final_map,legitimate_text)
                print(perturbed_text)
            else:
                print("WRONG input given! Please re-run the program and try again!")
        elif (choice==6):
            if choice2 == 1:
                perturbed_text = insert_space_and_perturbed_neg(final_map, legitimate_text)
                print(perturbed_text)
            elif choice2 == 2:
                perturbed_text=insert_space_and_perturbed_pos(final_map,legitimate_text)
                print(perturbed_text)
            else:
                print("WRONG input given! Please re-run the program and try again!")
        elif (choice==7):
            if choice2 == 1:
                perturbed_text = replace_small_length_words_neg(final_map, legitimate_text)
                print(perturbed_text)
            elif choice2 == 2:
                perturbed_text = replace_small_length_words_pos(final_map, legitimate_text)
                print(perturbed_text)
            else:
                print("WRONG input given! Please re-run the program and try again!")
        else:
            print("WRONG input given! Please re-run the program and try again!")
            exit(0)
    print("Thanks for evading NLP machines with EvilText! Hope to see you soon! :)")

# EXECUTE PROGRAM
main()