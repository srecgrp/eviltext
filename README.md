# EvilText
This repository contains the source code for the paper titled: **"Unicode Evil: Evading NLP Systems Using Visual Similarities of Text Characters".**

## Usage
`python EvilText.py`

## Dependencies
The code is written in python 3.

## Citing this work 
If you use this repository for academic research, you are highly encouraged (though not required) to cite our paper:

```
@inproceedings{dionysiou2021unicode,
  title={Unicode Evil: Evading NLP Systems Using Visual Similarities of Text Characters},
  author={Dionysiou, Antreas and Athanasopoulos, Elias},
  booktitle={Proceedings of the 14th ACM Workshop on Artificial Intelligence and Security},
  pages={1--12},
  year={2021}
}
```

## Authors
- Antreas Dionysiou - adiony01@cs.ucy.ac.cy (corresponding author)
- Elias Athanasopoulos - eliasathan@cs.ucy.ac.cy

Antreas and Elias are with the University of Cyprus (UCY) and members of the [SREC group](http://srec.cs.ucy.ac.cy/).


